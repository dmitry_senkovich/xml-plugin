package com.proop.lab5.xmlplugin;

import com.proop.lab4.json.JsonUnmarshaller;
import com.proop.lab4.shape.Shape;
import com.proop.lab5.xmlplugin.xml.XmlMarshaller;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.xml.transform.TransformerException;

import static javax.swing.JFileChooser.APPROVE_OPTION;

public class XmlPluginForm extends JPanel {

    private XmlMarshaller xmlMarshaller = new XmlMarshaller();
    private JsonUnmarshaller jsonUnmarshaller = new JsonUnmarshaller();

    List<Shape> shapes;

    public XmlPluginForm(File file) throws IOException {
        initComponents();
        loadFromFile(file);
    }

    private void loadFromFile(File file) throws IOException {
        try {
            shapes = jsonUnmarshaller.read(file);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, String.format("Failed to load shapes from file: %s", file.getName()), "Error while loading shapes",  JOptionPane.ERROR_MESSAGE);
            throw e;
        }
    }

    private void saveToFile(MouseEvent e) {
        String xslt = xsltTextArea.getText();
        JFileChooser fileChooser = new JFileChooser();
        int returnValue = fileChooser.showSaveDialog(this);
        if (APPROVE_OPTION == returnValue) {
            try {
                xmlMarshaller.save(fileChooser.getSelectedFile(), shapes, xslt);
                JOptionPane.showMessageDialog(this, "Transformation is saved!");
            } catch (IOException exception) {
                System.out.println("Failed to save shapes");
                JOptionPane.showMessageDialog(this,
                        "Failed to save shapes",
                        "Error while saving shapes",  JOptionPane.ERROR_MESSAGE);
            } catch (TransformerException exception) {
                System.out.println("Failed to transform shapes");
                JOptionPane.showMessageDialog(this,
                        "Failed to transform shapes",
                        "Error while transforming shapes",  JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void initComponents() {
        setLayout(null);
        setPreferredSize(new Dimension(250, 200));

        xsltTextArea = new JTextArea();
        add(xsltTextArea);
        xsltTextArea.setBounds(10, 10, 230, 140);

        saveButton = new JButton();
        saveButton.setText("Save");
        saveButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                saveToFile(e);
            }
        });
        add(saveButton);
        saveButton.setBounds(75, 160, 100, 30);
    }

    private JTextArea xsltTextArea;
    private JButton saveButton;
}
