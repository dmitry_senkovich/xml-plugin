package com.proop.lab5.xmlplugin;

import com.proop.lab4.plugin.ShapeMenu;

import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import static javax.swing.JFileChooser.APPROVE_OPTION;

public class XmlPluginMenu extends ShapeMenu {

    private JFrame parentFrame;

    public XmlPluginMenu() {
        setText("XML");
        add(createLoadMenuItem());
    }

    private JMenuItem createLoadMenuItem() {
        JMenuItem loadMenuItem = new JMenuItem("Load");
        loadMenuItem.addActionListener(e -> {
            JFileChooser jFileChooser = new JFileChooser();
            int returnValue = jFileChooser.showOpenDialog(this);
            if (APPROVE_OPTION == returnValue) {
                File file = jFileChooser.getSelectedFile();
                try {
                    JPanel shapeOfYouForm = new XmlPluginForm(file);
                    parentFrame.getContentPane().removeAll();
                    parentFrame.add(shapeOfYouForm);
                } catch (IOException | NoSuchElementException exception) {}
                parentFrame.pack();
                parentFrame.revalidate();
            }
        });

        return loadMenuItem;
    }

    @Override
    public void setParentFrame(JFrame parentFrame) {
        this.parentFrame = parentFrame;
    }
}
