package com.proop.lab5.xmlplugin.xml;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.proop.lab4.shape.Shape;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XmlMarshaller {

    private static final XmlMapper XML_MAPPER = new XmlMapper();
    static {
        XML_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
        XML_MAPPER.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        XML_MAPPER.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        XML_MAPPER.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
    }

    public void save(File file, List<Shape> shapes, String xslt) throws IOException, TransformerException {
        try {
            String xml = XML_MAPPER.writeValueAsString(shapes);
            if (StringUtils.isBlank(xslt)) {
                FileUtils.writeStringToFile(file, xml, Charset.defaultCharset());
            } else {
                transform(xml, xslt, file);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to save shapes to file: " + file.getName());
            throw e;
        } catch (TransformerException e) {
            e.printStackTrace();
            System.out.println("Failed to transform shapes xml");
            throw e;
        }
    }

    private void transform(String xml, String xslt, File file) throws TransformerException {
        TransformerFactory factory = TransformerFactory.newInstance();
        StreamSource xslStream = new StreamSource(new StringReader(xslt));
        Transformer transformer = factory.newTransformer(xslStream);
        StreamSource in = new StreamSource(new StringReader(xml));
        StreamResult out = new StreamResult(file);
        transformer.transform(in, out);
    }

}
